import { Exclude, Expose } from 'class-transformer';
import { Employee } from 'src/employees/entities/employee.entity';
import { Inventory } from 'src/inventory/entities/inventory.entity';
import { StockTakingItem } from 'src/stock-taking/entities/stock-taking-item.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class StockTaking {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdDate: Date;

  @Column({
    default: new Date().toLocaleTimeString('th-TH', { hour12: false }),
  })
  createdTime: string;

  @OneToMany(() => StockTakingItem, (stk) => stk.stockTaking, {
    cascade: ['insert', 'update', 'remove'],
    onDelete: 'CASCADE',
    eager: true,
  })
  stockTakingItems: StockTakingItem[];

  @ManyToOne(() => Employee, (emp) => emp.stockTakingList, { eager: true })
  @JoinColumn()
  @Exclude()
  employee: Employee;

  @ManyToOne(() => Inventory, (inv) => inv.stockTakingList, { eager: true })
  @Exclude()
  @JoinColumn()
  inventory: Inventory;

  @Expose()
  get employeeName(): string {
    return `${this.employee?.firstName} ${this.employee?.lastName}`;
  }

  @Expose()
  get inventoryName(): string {
    return this.inventory?.name;
  }
}
