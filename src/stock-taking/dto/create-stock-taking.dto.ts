import { IsArray, IsNumber } from 'class-validator';
import { CreateStockTakingItemDto } from 'src/stock-taking/dto/create-stock-taking-item.dto';

export class CreateStockTakingDto {
  @IsArray()
  stockTakingItems: CreateStockTakingItemDto[];

  @IsNumber()
  employeeId: number;

  @IsNumber()
  inventoryId: number;
}
