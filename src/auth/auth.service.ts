import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { User } from 'src/users/entities/user.entity';
@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(email: string, pass: string): Promise<any> {
    let user: User;
    try {
      user = await this.usersService.findOneByEmail(email);
    } catch {
      throw new NotFoundException('User not found');
    }
    if (!bcrypt.compareSync(pass, user.password)) {
      throw new UnauthorizedException('Password incorrect');
    }

    const payload = { id: user.id, email: user.email };
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, employee, customer, ...result } = user;
    return {
      user: {
        ...result,
        fullName: user.fullName,
        image: user.image,
        empId: employee?.id,
        cusId: customer?.id,
        branch: {
          id: employee?.branch.id,
          name: employee?.branch.name,
        },
      },
      access_token: await this.jwtService.signAsync(payload),
    };
  }
}
