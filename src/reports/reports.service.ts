/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Repository } from 'typeorm';
import { DateFilterDTO } from './dto/revenue.dto';
import { Bill } from 'src/bills/entities/bill.entity';

@Injectable()
export class ReportsService {
  constructor(
    @InjectRepository(Receipt)
    private readonly repoReceipt: Repository<Receipt>,
    @InjectRepository(Bill)
    private readonly repoBill: Repository<Bill>,
  ) {}

  //REVENUE
  async getRevenueDaily(dto: DateFilterDTO) {
    try {
      const startDate = new Date(dto.year, dto.month - 1, 1);
      const endDate = new Date(
        startDate.getFullYear(),
        startDate.getMonth() + 1,
        0,
      );
      const queryBuilder = this.repoReceipt
        .createQueryBuilder('receipt')
        .select('SUM(receipt.netPrice)', 'net')
        .addSelect('DAY(receipt.createdDateTime)', 'date')
        .where('(receipt.createdDateTime) BETWEEN :startDate AND :endDate', {
          startDate: startDate.toISOString().slice(0, 10),
          endDate: endDate.toISOString().slice(0, 10),
        })
        .groupBy('date')
        .orderBy('date');
      if (dto.branchId) {
        queryBuilder.andWhere('receipt.branchId = :id', { id: dto.branchId });
      }
      return queryBuilder.getRawMany();
    } catch (error) {
      console.log('getDaily Error: ' + error);
    }
  }
  async getRevenueMonthly(dto: DateFilterDTO) {
    try {
      const startDate = new Date(dto.year, 0, 1);
      const endDate = new Date(dto.year, 11, 31);
      const queryBuilder = this.repoReceipt
        .createQueryBuilder('receipt')
        .select("DATE_FORMAT(receipt.createdDateTime,'%m')", 'month')
        .addSelect('SUM(receipt.netPrice)', 'net')
        .where('receipt.createdDateTime BETWEEN :startDate AND :endDate', {
          startDate: startDate.toISOString().slice(0, 10),
          endDate: endDate.toISOString().slice(0, 10),
        })
        .groupBy('month')
        .orderBy('month');
      if (dto.branchId) {
        queryBuilder.andWhere('receipt.branchId = :id', { id: dto.branchId });
      }
      return queryBuilder.getRawMany();
    } catch (error) {
      console.log('getMonthly Error: ' + error);
    }
  }

  async getRevenueYearly(dto: DateFilterDTO) {
    try {
      const queryBuilder = this.repoReceipt
        .createQueryBuilder('receipt')
        .select("DATE_FORMAT(receipt.createdDateTime,'%Y')", 'year')
        .addSelect('SUM(receipt.netPrice)', 'net')
        .groupBy('year')
        .orderBy('year');
      if (dto.branchId) {
        queryBuilder.andWhere('receipt.branchId = :id', { id: dto.branchId });
      }
      return queryBuilder.getRawMany();
    } catch (error) {
      console.log('getYearly Error :' + error);
    }
  }
  //EXPENSE
  async getExpenseDaily(dto: DateFilterDTO) {
    try {
      const startDate = new Date(dto.year, dto.month - 1, 1);
      const endDate = new Date(
        startDate.getFullYear(),
        startDate.getMonth() + 1,
        0,
      );
      const queryBuilder = this.repoBill
        .createQueryBuilder('bill')
        .select('SUM(bill.worth)', 'net')
        .addSelect('DAY(bill.createdDate)', 'date')
        .where('(bill.createdDate) BETWEEN :startDate AND :endDate', {
          startDate: startDate.toISOString().slice(0, 10),
          endDate: endDate.toISOString().slice(0, 10),
        })
        .groupBy('date')
        .orderBy('date');
      if (dto.branchId) {
        queryBuilder.andWhere('bill.branchId = :id', { id: dto.branchId });
      }
      return queryBuilder.getRawMany();
    } catch (error) {
      throw error;
    }
  }
  async getExpenseMonthly(dto: DateFilterDTO) {
    try {
      const startDate = new Date(dto.year, 0, 1);
      const endDate = new Date(dto.year, 11, 31);
      const queryBuilder = this.repoBill
        .createQueryBuilder('bill')
        .select("DATE_FORMAT(bill.createdDate,'%m')", 'month')
        .addSelect('SUM(bill.worth)', 'net')
        .where('bill.createdDate BETWEEN :startDate AND :endDate', {
          startDate: startDate.toISOString().slice(0, 10),
          endDate: endDate.toISOString().slice(0, 10),
        })
        .groupBy('month')
        .orderBy('month');
      if (dto.branchId) {
        queryBuilder.andWhere('receipt.branchId = :id', { id: dto.branchId });
      }
      return queryBuilder.getRawMany();
    } catch (error) {
      throw error;
    }
  }

  async getExpenseYearly(dto: DateFilterDTO) {
    try {
      const queryBuilder = this.repoBill
        .createQueryBuilder('bill')
        .select("DATE_FORMAT(bill.createdDate,'%Y')", 'year')
        .addSelect('SUM(bill.worth)', 'net')
        .groupBy('year')
        .orderBy('year');
      if (dto.branchId) {
        queryBuilder.andWhere('bill.branchId = :id', { id: dto.branchId });
      }
      return queryBuilder.getRawMany();
    } catch (error) {
      throw error;
    }
  }
  //LAST SALES
  async getLastDayAndCurrentSold(branchId?: number) {
    try {
      const pastQueryBuilder = this.repoReceipt
        .createQueryBuilder('sales')
        .select('SUM(sales.netPrice)', 'pastTotal')
        .where('DAY(sales.createdDateTime) = DAY(CURDATE()) - 1');

      const nowQueryBuilder = this.repoReceipt
        .createQueryBuilder('sales')
        .select('SUM(sales.netPrice)', 'nowTotal')
        .where('DAY(sales.createdDateTime) = DAY(CURDATE())');

      if (branchId) {
        pastQueryBuilder.andWhere('sales.branchId = :branchId', { branchId });
        nowQueryBuilder.andWhere('sales.branchId = :branchId', { branchId });
      }

      const [pastResult, currentResult] = await Promise.all([
        pastQueryBuilder.getRawOne(),
        nowQueryBuilder.getRawOne(),
      ]);

      const pastTotal = pastResult ? pastResult.pastTotal : 0;
      const currentTotal = currentResult ? currentResult.nowTotal : 0;

      return { yesterday: pastTotal, today: currentTotal };
    } catch (error) {
      console.error('Error retrieving sales data:', error);
      throw new Error('Failed to retrieve sales data');
    }
  }

  async getLastMonthAndCurrentSold(
    branchId?: number,
  ): Promise<{ lastMonth: number; thisMonth: number }> {
    const nowDate = new Date();

    try {
      const pastDate = new Date(
        nowDate.getFullYear(),
        nowDate.getMonth() - 1,
        1,
      );
      const pastMonthQueryBuilder = this.repoReceipt
        .createQueryBuilder('sales')
        .select('SUM(sales.netPrice)', 'pastTotal')
        .where('MONTH(sales.createdDateTime) = :past', {
          past: pastDate.getMonth(),
        });

      const currentMonthQueryBuilder = this.repoReceipt
        .createQueryBuilder('sales')
        .select('SUM(sales.netPrice)', 'nowTotal')
        .where('MONTH(sales.createdDateTime) = :now', {
          now: nowDate.getMonth() + 1,
        });

      if (branchId) {
        pastMonthQueryBuilder.andWhere('sales.branchId = :branchId', {
          branchId,
        });
        currentMonthQueryBuilder.andWhere('sales.branchId = :branchId', {
          branchId,
        });
      }

      const [pastResult, currentResult] = await Promise.all([
        pastMonthQueryBuilder.getRawOne(),
        currentMonthQueryBuilder.getRawOne(),
      ]);

      const pastTotal = pastResult.pastTotal;
      const currentTotal = currentResult.nowTotal;

      return { lastMonth: pastTotal, thisMonth: currentTotal };
    } catch (error) {
      console.error('Error retrieving sales data:', error);
      throw error; // Re-throw the original error for better handling
    }
  }

  async getLastYearAndCurrentSold(
    branchId?: number,
  ): Promise<{ lastYear: number; thisYear: number }> {
    const currentYear = new Date().getFullYear();

    const pastYearQuery = this.repoReceipt
      .createQueryBuilder('sales')
      .where('YEAR(sales.createdDateTime) = :pastYear', {
        pastYear: currentYear - 1,
      })
      .select('SUM(sales.netPrice)', 'pastTotal');

    const currentYearQuery = this.repoReceipt
      .createQueryBuilder('sales')
      .where('YEAR(sales.createdDateTime) = :currentYear', { currentYear })
      .select('SUM(sales.netPrice)', 'nowTotal');

    if (branchId) {
      pastYearQuery.andWhere('sales.branchId = :branchId', { branchId });
      currentYearQuery.andWhere('sales.branchId = :branchId', { branchId });
    }

    const pastTotal = await pastYearQuery.getRawOne();
    const currentTotal = await currentYearQuery.getRawOne();

    return { lastYear: pastTotal.pastTotal, thisYear: currentTotal.nowTotal };
  }
  //VIEW EXPENSE
  getViewExpenseSum() {
    return this.repoReceipt.query(
      'SELECT branch, SUM(total_worth) AS totalWorth FROM reportExpenseSum GROUP BY branch',
    );
  }
  getViewExpenseBranch(id: number) {
    return this.repoBill
      .createQueryBuilder()
      .select('name')
      .addSelect('SUM(worth)', 'totalWorth')
      .where('branchId = :id', { id: id })
      .groupBy('name')
      .getRawMany();
  }
  getViewTopProducts(top: boolean) {
    if (top)
      return this.repoBill.query(
        'SELECT * from report_products_sold ORDER BY totalPrice DESC LIMIT 5',
      );
    return this.repoBill.query(
      'SELECT * from report_products_sold ORDER BY totalPrice ASC LIMIT 5',
    );
  }
  getViewIngredients() {
    return this.repoBill.query('SELECT * FROM report_ingredients');
  }
}
