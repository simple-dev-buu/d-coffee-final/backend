import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ReportsService } from './reports.service';
import { DateFilterDTO } from './dto/revenue.dto';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @Post('revenue')
  async getRevenue(@Body() dto: DateFilterDTO) {
    return {
      day: await this.reportsService.getRevenueDaily(dto),
      month: await this.reportsService.getRevenueMonthly(dto),
      year: await this.reportsService.getRevenueYearly(dto),
    };
  }

  @Post('expense')
  async getExpense(@Body() dto: DateFilterDTO) {
    return {
      day: await this.reportsService.getExpenseDaily(dto),
      month: await this.reportsService.getExpenseMonthly(dto),
      year: await this.reportsService.getExpenseYearly(dto),
    };
  }

  @Get('expense-sum')
  getExpenseSum() {
    return this.reportsService.getViewExpenseSum();
  }

  @Post('expense-branch/:id')
  getExpenseBranch(@Param('id') id: string) {
    return this.reportsService.getViewExpenseBranch(+id);
  }

  @Get('ingredients')
  getReportIngredients() {
    return this.reportsService.getViewIngredients();
  }
  @Post('top-products/:top')
  getReportTopProducts(@Param('top') top: number) {
    if (top > 0) {
      return this.reportsService.getViewTopProducts(true);
    }
    return this.reportsService.getViewTopProducts(false);
  }

  @Get('sales')
  async getSales() {
    return {
      dailySales: await this.reportsService.getLastDayAndCurrentSold(),
      monthlySales: await this.reportsService.getLastMonthAndCurrentSold(),
      yearlySales: await this.reportsService.getLastYearAndCurrentSold(),
    };
  }

  @Get('sales/:branchId')
  async getDashboardByBranchId(@Param('branchId') branchId: string) {
    return {
      dailySales: await this.reportsService.getLastDayAndCurrentSold(+branchId),
      monthlySales:
        await this.reportsService.getLastMonthAndCurrentSold(+branchId),
      yearlySales:
        await this.reportsService.getLastYearAndCurrentSold(+branchId),
    };
  }
}
