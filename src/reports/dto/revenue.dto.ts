import { IsInt, IsOptional } from 'class-validator';

export class DateFilterDTO {
  @IsInt()
  @IsOptional()
  branchId: number;

  @IsInt()
  month: number;

  @IsInt()
  year: number;
}
