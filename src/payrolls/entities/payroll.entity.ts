import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { PayrollDetail } from 'src/payroll-detail/entities/payroll-detail.entity';
import { Expose } from 'class-transformer';

@Entity()
export class Payroll {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  totalNet: number;

  @Column()
  totalDeduction: number;

  @Column()
  payrollStatus: string;

  @CreateDateColumn()
  createdDate: Date;

  @ManyToOne(() => Employee, (employee) => employee.payrolls, {
    eager: true,
  })
  @JoinColumn()
  employee: Employee;

  @OneToOne(() => PayrollDetail, (payrollDetail) => payrollDetail.payroll, {
    cascade: ['insert', 'remove', 'update'],
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
    eager: true,
  })
  @JoinColumn()
  payrollDetail: PayrollDetail;
}
