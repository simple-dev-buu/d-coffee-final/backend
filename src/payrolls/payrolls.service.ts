import { Injectable } from '@nestjs/common';
import { CreatePayrollDto } from './dto/create-payroll.dto';
import { UpdatePayrollDto } from './dto/update-payroll.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Payroll } from './entities/payroll.entity';
import { PayrollDetail } from 'src/payroll-detail/entities/payroll-detail.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Injectable()
export class PayrollsService {
  constructor(
    @InjectRepository(Payroll)
    private readonly payrollRepository: Repository<Payroll>,
    @InjectRepository(PayrollDetail)
    private readonly payrollDetailRepository: Repository<PayrollDetail>,
    @InjectRepository(Employee)
    private readonly employeeRepository: Repository<Employee>,
  ) {}
  async create(createPayrollDto: CreatePayrollDto[]) {
    try {
      for (const createPayroll of createPayrollDto) {
        const emp = await this.employeeRepository.findOne({
          where: { id: createPayroll.employee.id },
        });

        const payDetail = this.payrollDetailRepository.create(
          createPayroll.payrollDetail,
        );
        const payroll = this.payrollRepository.create({
          ...createPayroll,
          payrollDetail: payDetail,
        });
        payroll.employee = emp;
        payroll.payrollStatus = 'Paid';

        console.log('payroll', payroll);
        await this.payrollRepository.save(payroll);
      }
    } catch (error) {
      console.log('backend payroll error', error);
    }
  }

  findAll() {
    return this.payrollRepository.find();
  }

  findOne(id: number) {
    const payroll = this.payrollRepository.findOneBy({ id: id });
    return payroll;
  }

  async update(id: number, updatePayrollDto: UpdatePayrollDto) {
    return this.payrollRepository.update(id, updatePayrollDto);
  }

  async remove(id: number) {
    const deletePayroll = await this.payrollRepository.delete(id);
    return deletePayroll;
  }
}
