import { IsNumber, IsString } from 'class-validator';
import { Employee } from 'src/employees/entities/employee.entity';
import { CreatePayrollDetailDto } from 'src/payroll-detail/dto/create-payroll-detail.dto';

export class CreatePayrollDto {
  @IsNumber()
  totalNet: number;

  @IsNumber()
  totalDeduction: number;

  @IsString()
  payrollStatus: string;

  employee: Employee;

  payrollDetail: CreatePayrollDetailDto;
}
