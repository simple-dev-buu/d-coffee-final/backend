import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BillsModule } from './bills/bills.module';
import { CustomersModule } from './customers/customers.module';
import { ProductsModule } from './products/products.module';
import { PromotionsModule } from './promotions/promotions.module';
import { UsersModule } from './users/users.module';
import { EmployeesModule } from './employees/employees.module';
import { CategoriesModule } from './categories/categories.module';
import { RolesModule } from './roles/roles.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { InventoryModule } from './inventory/inventory.module';
import { StockTakingModule } from './stock-taking/stock-taking.module';
import { ReplenishmentModule } from './replenishment/replenishment.module';
import { IngredientsModule } from './ingredients/ingredients.module';
import { BranchModule } from './branches/branch.module';
import { AuthModule } from './auth/auth.module';
import { ReceiptsModule } from './receipts/receipts.module';
import { PayrollsModule } from './payrolls/payrolls.module';
import { AttendancesModule } from './attendances/attendances.module';
import { PayrollDetailModule } from './payroll-detail/payroll-detail.module';
import { ReportsModule } from './reports/reports.module';

let publicPath = join(__dirname, '..', 'public');
const isMacOS = process.platform === 'darwin';

if (isMacOS) {
  publicPath = join(__dirname, '..', '../public');
}

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: publicPath,
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'angsila.informatics.buu.ac.th',
      port: 3306,
      username: 'cscamp10',
      password: '5b1kZLTj9I',
      database: 'cscamp10',
      synchronize: true,
      autoLoadEntities: true,
      logging: true,
    }),
    ReportsModule,
    BillsModule,
    CustomersModule,
    ProductsModule,
    PromotionsModule,
    UsersModule,
    EmployeesModule,
    PromotionsModule,
    CategoriesModule,
    RolesModule,
    InventoryModule,
    StockTakingModule,
    ReplenishmentModule,
    IngredientsModule,
    AuthModule,
    ReceiptsModule,
    PayrollsModule,
    AttendancesModule,
    BranchModule,
    PayrollDetailModule,
    PayrollDetailModule,
  ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
