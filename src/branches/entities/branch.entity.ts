import { Bill } from 'src/bills/entities/bill.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Inventory } from 'src/inventory/entities/inventory.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  location: string;

  @CreateDateColumn()
  createdDate: Date;

  @OneToOne(() => Inventory, {
    cascade: ['insert', 'remove', 'update'],
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
    eager: true,
  })
  @JoinColumn()
  inventory: Inventory;

  @OneToMany(() => Receipt, (rec) => rec.branch)
  receipts: Receipt[];

  @OneToMany(() => Employee, (emp) => emp.branch)
  employees: Employee[];

  @OneToMany(() => Bill, (bill) => bill.branch)
  bills: Bill[];
}
