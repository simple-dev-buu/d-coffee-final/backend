import { Employee } from 'src/employees/entities/employee.entity';
import { Role } from 'src/roles/entities/role.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToOne,
  BeforeInsert,
} from 'typeorm';
import { hashPassword } from 'src/utils/hash';
import { Exclude, Expose } from 'class-transformer';
import { Customer } from 'src/customers/entities/customer.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @BeforeInsert()
  async hashPassword() {
    this.password = await hashPassword(this.password);
  }

  @CreateDateColumn()
  createdDate: Date;

  @ManyToOne(() => Role, (role) => role.users, { eager: true })
  @JoinColumn()
  role: Role;

  @OneToOne(() => Employee, { nullable: true, eager: true })
  @Exclude()
  @JoinColumn()
  employee: Employee;

  @OneToOne(() => Customer, { nullable: true, eager: true })
  @Exclude()
  @JoinColumn()
  customer: Customer;

  @Expose()
  get image() {
    if (this.employee) {
      return this.employee?.image;
    }
    return this.customer?.image;
  }

  @Expose()
  get fullName(): string {
    if (this.employee) {
      return `${this.employee?.firstName} ${this.employee?.lastName}`;
    }
    return `${this.customer?.name}`;
  }
}
