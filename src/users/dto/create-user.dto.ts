import { IsEmail, IsInt, IsOptional, IsString } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  email: string;
  @IsString()
  password: string;
  @IsString()
  roleName: string;
  @IsInt()
  @IsOptional()
  empId: number;
  @IsInt()
  @IsOptional()
  cusId: number;
}
