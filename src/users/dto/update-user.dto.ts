import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
import { IsString, IsEmail, IsInt, IsOptional } from 'class-validator';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @IsEmail()
  email: string;
  @IsString()
  password: string;
  @IsString()
  roleName: string;
  @IsInt()
  @IsOptional()
  empId: number;
  @IsInt()
  @IsOptional()
  cusId: number;
}
