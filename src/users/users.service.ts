import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/roles/entities/role.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Customer } from 'src/customers/entities/customer.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
    @InjectRepository(Employee) private repoEmp: Repository<Employee>,
    @InjectRepository(Customer) private repoCus: Repository<Customer>,
  ) {}
  async create(dto: CreateUserDto) {
    const role = await this.rolesRepository.findOneByOrFail({
      name: dto.roleName,
    });
    let emp: Employee;
    if (dto.empId) {
      emp = await this.repoEmp.findOneBy({ id: dto.empId });
    }
    let cus: Customer;
    if (dto.cusId) {
      cus = await this.repoCus.findOneBy({ id: dto.cusId });
    }
    const user = this.usersRepository.create({
      ...dto,
      role: role,
      employee: emp,
      customer: cus,
    });

    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({
      relations: { role: true },
    });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { role: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneByOrFail({ email });
  }

  async update(id: number, dto: UpdateUserDto) {
    const user = await this.usersRepository.findOneByOrFail({ id });
    if (dto.email) {
      user.email = dto.email;
    }
    if (dto.password) {
      user.password = dto.password;
    }
    if (dto.roleName) {
      const role = await this.rolesRepository.findOneBy({
        name: dto.roleName,
      });
      user.role = role;
    }
    if (dto.empId) {
      const emp = await this.repoEmp.findOneBy({ id: dto.empId });
      user.employee = emp;
    }
    const updateUser = await this.usersRepository.save(user);
    return updateUser;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneByOrFail({ id });
    await this.usersRepository.remove(deleteUser);
    return deleteUser;
  }
}
