import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { PromotionsService } from './promotions.service';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { uuid } from 'uuidv4';
import { extname } from 'path';

@Controller('promotions')
export class PromotionsController {
  constructor(private readonly promotionsService: PromotionsService) {}

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/promotions',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body('promotion') dto: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(file);
    console.log(dto);
    const promotion: CreatePromotionDto = JSON.parse(dto);
    if (file) {
      promotion.image = file.filename;
    }
    return this.promotionsService.create(promotion);
  }

  @Get()
  findAll() {
    return this.promotionsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.promotionsService.findOne(+id);
  }

  @Post(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/promotions',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updatePromotionDto: UpdatePromotionDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(file);
    console.log(updatePromotionDto);
    if (file) {
      updatePromotionDto.image = file.filename;
    }
    return this.promotionsService.update(+id, updatePromotionDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.promotionsService.remove(+id);
  }
}
