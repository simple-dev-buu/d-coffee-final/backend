import {
  IsArray,
  IsBoolean,
  IsDate,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreatePromotionDto {
  @IsString()
  name: string;
  @IsString()
  description: string;
  @IsNumber()
  discount: number;
  @IsBoolean()
  comboSet: boolean;
  @IsDate()
  startDate: Date;
  @IsDate()
  endDate: Date;
  @IsBoolean()
  status: boolean;
  @IsString()
  @IsOptional()
  image: string;
  @IsArray()
  usableProducts: number[];
}
