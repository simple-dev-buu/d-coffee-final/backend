import { Product } from 'src/products/entities/product.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  OneToMany,
  JoinTable,
} from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  discount: number;

  @Column()
  comboSet: boolean;

  @Column()
  startDate: Date;

  @Column()
  endDate: Date;

  @Column()
  status: boolean;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @ManyToMany(() => Product, (product) => product.promotions)
  @JoinTable()
  productsUsable: Product[];

  @OneToMany(() => Receipt, (receipt) => receipt.promotion)
  receipt: Receipt[];
}
