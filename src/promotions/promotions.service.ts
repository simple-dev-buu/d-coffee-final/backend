import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Promotion } from './entities/promotion.entity';
import { Product } from 'src/products/entities/product.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PromotionsService {
  constructor(
    @InjectRepository(Promotion)
    private promotionRepository: Repository<Promotion>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  async create(createPromotionDto: CreatePromotionDto) {
    const promotion = this.promotionRepository.create({
      ...createPromotionDto,
    });

    if (createPromotionDto.image && createPromotionDto.image !== '') {
      promotion.image = createPromotionDto.image;
    }

    console.log('Start Mapping UsableProduct');
    const products = await Promise.all(
      (createPromotionDto.usableProducts || []).map(async (productId) => {
        return await this.productsRepository.findOneBy({ id: productId });
      }),
    );
    console.log('Finish Mapping UsableProduct');

    console.log(JSON.stringify(promotion));
    promotion.productsUsable = products;

    return this.promotionRepository.save(promotion);
  }
  findAll() {
    return this.promotionRepository.find();
  }

  async findAllByProductId(productId: number) {
    const product = await this.productsRepository.findOneBy({
      id: productId,
    });
    return this.promotionRepository.find({
      where: { productsUsable: product },
      relations: { productsUsable: true },
      order: { name: 'DESC' },
    });
  }

  findOne(id: number) {
    return this.promotionRepository.findOneBy({ id: id });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    const updatePromotion = await this.promotionRepository.findOneBy({ id });
    updatePromotion.name = updatePromotionDto.name;
    updatePromotion.description = updatePromotionDto.description;
    updatePromotion.discount = updatePromotionDto.discount;
    updatePromotion.comboSet = Boolean(updatePromotionDto.comboSet);
    updatePromotion.startDate = updatePromotionDto.startDate;
    updatePromotion.endDate = updatePromotionDto.endDate;
    updatePromotion.status = Boolean(updatePromotionDto.status);
    if (updatePromotionDto.image && updatePromotionDto.image !== '') {
      updatePromotion.image = updatePromotionDto.image;
    }
    this.promotionRepository.save(updatePromotion);

    const result = await this.promotionRepository.findOne({
      where: { id: id },
    });
    return result;
  }

  async remove(id: number) {
    const deletePromotion = await this.promotionRepository.findOneBy({ id });
    return this.promotionRepository.remove(deletePromotion);
  }
}
