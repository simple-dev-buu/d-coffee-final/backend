import { IsInt, IsNumber, IsPositive, IsString } from 'class-validator';

export class CreateBillDto {
  @IsString()
  name: string;

  @IsNumber()
  @IsPositive()
  worth: number;

  @IsInt()
  branch_id: number;

  @IsInt()
  emp_id: number;
}
