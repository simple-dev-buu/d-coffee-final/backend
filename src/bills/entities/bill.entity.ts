/* eslint-disable prettier/prettier */
import { Transform } from 'class-transformer';
import { Branch } from 'src/branches/entities/branch.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @CreateDateColumn()
  createdDate: Date;

  @Column()
  worth: number;

  @ManyToOne(() => Branch, (branch) => branch.bills, { eager: true })
  @JoinColumn()
  @Transform(({ value }) => ({ name: value.name, id: value.id }))
  branch: Branch;

  @ManyToOne(() => Employee, (employee) => employee.bills)
  @JoinColumn()
  employee: Employee;
}
