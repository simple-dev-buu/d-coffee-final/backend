/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Bill } from './entities/bill.entity';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bill)
    private BillRepository: Repository<Bill>,
  ) {}

  create(createBillDto: CreateBillDto) {
    const bill = new Bill();
    bill.name = createBillDto.name;
    bill.worth = createBillDto.worth;
    return this.BillRepository.save(bill);
  }

  findAll() {
    return this.BillRepository.find();
  }

  findOne(id: number) {
    return this.BillRepository.findOneBy({ id: id });
  }

  async findBillThisMonth(
    billType: string,
    targetMonth: number,
    specificBranch: number,
    targetYear: number,
  ) {
    return await this.BillRepository.query(
      `CALL billThisMonth('${billType}', ${targetMonth}, ${specificBranch}, ${targetYear})`,
    );
  }

  findAllWithFilter(year: number, month: number) {
    const startMonth = () => {
      if (month === 0 || month === undefined) {
        return 0;
      }
      return month - 1;
    };
    const endMonth = () => {
      if (month === 0 || month === undefined) {
        return 11;
      }
      return month;
    };
    return this.BillRepository.createQueryBuilder('bill')
      .select('bill')
      .where('bill.createdDate BETWEEN :startDate AND :endDate', {
        startDate: new Date(year, startMonth(), 1).toLocaleDateString('fr-CA'),
        endDate: new Date(year, endMonth(), 0).toLocaleDateString('fr-CA'),
      })
      .orderBy('createdDate', 'DESC')

      .getMany();
  }

  findSumWithFilter(name: string, year: number, month: number) {
    const startMonth = () => {
      if (month === 0 || month === undefined) {
        return 0;
      }
      return month - 1;
    };
    const endMonth = () => {
      if (month === 0 || month === undefined) {
        return 11;
      }
      return month;
    };
    return this.BillRepository.createQueryBuilder('b')
      .select('SUM(b.worth)', 'total')
      .where('b.createdDate BETWEEN :startDate AND :endDate')
      .andWhere('b.name = :name')
      .setParameters({
        startDate: new Date(year, startMonth(), 1),
        endDate: new Date(year, endMonth(), 0),
        name,
      })
      .getRawOne();
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    await this.BillRepository.update(id, updateBillDto);
    const bill = await this.BillRepository.findOneBy({ id: id });
    return bill;
  }

  async remove(id: number) {
    const delBill = await this.BillRepository.findOneByOrFail({ id: id });
    await this.BillRepository.remove(delBill);
    return delBill;
  }
}
