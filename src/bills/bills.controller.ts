import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { BillsService } from './bills.service';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';

@UseInterceptors(ClassSerializerInterceptor)
@Controller('bills')
export class BillsController {
  constructor(private readonly billsService: BillsService) {}

  @Post()
  create(@Body() createBillDto: CreateBillDto) {
    return this.billsService.create(createBillDto);
  }

  @Get()
  findAll() {
    return this.billsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.billsService.findOne(+id);
  }

  @Post('/query')
  findBillThisMonth(
    @Body()
    data: {
      billType: string;
      targetMonth: number;
      specificBranch: number;
      targetYear: number;
    },
  ) {
    return this.billsService.findBillThisMonth(
      data.billType,
      data.targetMonth,
      data.specificBranch,
      data.targetYear,
    );
  }

  @Get(':year/:month')
  findAllWithFilter(
    @Param('year') year: string,
    @Param('month') month: string,
  ) {
    return this.billsService.findAllWithFilter(+year, +month);
  }

  @Get('sum/:year/:month/:name')
  findSumWithFilter(
    @Param('year') year: string,
    @Param('month') month: string,
    @Param('name') name: string,
  ) {
    return this.billsService.findSumWithFilter(name, +year, +month);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBillDto: UpdateBillDto) {
    return this.billsService.update(+id, updateBillDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.billsService.remove(+id);
  }
}
