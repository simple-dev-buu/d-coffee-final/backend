import { Injectable } from '@nestjs/common';
import { CreatePayrollDetailDto } from './dto/create-payroll-detail.dto';
import { UpdatePayrollDetailDto } from './dto/update-payroll-detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Payroll } from 'src/payrolls/entities/payroll.entity';
import { Repository } from 'typeorm';
import { PayrollDetail } from './entities/payroll-detail.entity';
import { CreatePayrollDto } from 'src/payrolls/dto/create-payroll.dto';

@Injectable()
export class PayrollDetailService {
  constructor(
    @InjectRepository(Payroll)
    private readonly payrollRepository: Repository<Payroll>,
    @InjectRepository(PayrollDetail)
    private readonly payrollDetailRepository: Repository<PayrollDetail>,
  ) {}
  async create(createPayrollDto: CreatePayrollDto) {
    console.log(createPayrollDto);
    // try {

    // } catch (error) {
    //   console.log('backend payroll detail error', error);
    // }
  }

  findAll() {
    return `This action returns all payrollDetail`;
  }

  findOne(id: number) {
    return `This action returns a #${id} payrollDetail`;
  }

  update(id: number, updatePayrollDetailDto: UpdatePayrollDetailDto) {
    return `This action updates a #${id} payrollDetail`;
  }

  remove(id: number) {
    return `This action removes a #${id} payrollDetail`;
  }
}
