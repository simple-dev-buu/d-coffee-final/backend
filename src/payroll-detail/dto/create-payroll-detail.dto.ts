import { IsNumber } from 'class-validator';
import { Payroll } from 'src/payrolls/entities/payroll.entity';

export class CreatePayrollDetailDto {
  @IsNumber()
  salary: number;

  @IsNumber()
  overtime: number;

  @IsNumber()
  commission: number;

  @IsNumber()
  allowances: number;

  @IsNumber()
  bonus: number;

  @IsNumber()
  otherEarning: number;

  @IsNumber()
  socialSecurityFund: number;

  @IsNumber()
  withHoldingTax: number;

  @IsNumber()
  studentLoanFund: number;

  @IsNumber()
  deposit: number;

  @IsNumber()
  leave: number;

  @IsNumber()
  otherDeduction: number;

  @IsNumber()
  ytdEarning: number;

  @IsNumber()
  ytdWithHoldingTax: number;

  @IsNumber()
  collectSSF: number;

  @IsNumber()
  totalEarning: number;

  @IsNumber()
  totalDeduction: number;

  @IsNumber()
  netPay: number;

  payroll: Payroll;
}
