import { Module } from '@nestjs/common';
import { PayrollDetailService } from './payroll-detail.service';
import { PayrollDetailController } from './payroll-detail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PayrollDetail } from './entities/payroll-detail.entity';
import { Payroll } from 'src/payrolls/entities/payroll.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PayrollDetail, Payroll])],
  controllers: [PayrollDetailController],
  providers: [PayrollDetailService],
  exports: [PayrollDetailService],
})
export class PayrollDetailModule {}
