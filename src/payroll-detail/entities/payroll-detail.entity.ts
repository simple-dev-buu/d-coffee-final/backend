import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Payroll } from '../../payrolls/entities/payroll.entity';
@Entity()
export class PayrollDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  salary: number;

  @Column()
  overtime: number;

  @Column()
  commission: number;

  @Column()
  allowances: number;

  @Column()
  bonus: number;

  @Column()
  otherEarning: number;

  @Column()
  socialSecurityFund: number;

  @Column()
  withHoldingTax: number;

  @Column()
  studentLoanFund: number;

  @Column()
  deposit: number;

  @Column()
  leave: number;

  @Column()
  otherDeduction: number;

  @Column()
  ytdEarning: number;

  @Column()
  ytdWithHoldingTax: number;

  @Column()
  collectSSF: number;

  @Column()
  totalEarning: number;

  @Column()
  totalDeduction: number;

  @Column()
  netPay: number;

  @OneToOne(() => Payroll, (payroll) => payroll.payrollDetail)
  payroll: Payroll;
}
