import { Exclude, Expose } from 'class-transformer';
import { Employee } from 'src/employees/entities/employee.entity';
import { Inventory } from 'src/inventory/entities/inventory.entity';
import { ReplenishmentItem } from 'src/replenishment/entities/replenishment-items.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Replenishment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: new Date().toISOString().slice(0, 10) })
  createdDate: string;

  @ManyToOne(() => Employee, (employee) => employee.replenishment)
  @JoinColumn()
  @Exclude({ toPlainOnly: true })
  employee: Employee;

  @Column()
  totalCost: number;

  @OneToMany(() => ReplenishmentItem, (rep) => rep.replenishment, {
    cascade: ['insert', 'update', 'remove'],
    onDelete: 'CASCADE',
    eager: true,
  })
  replenishmentItems: ReplenishmentItem[];

  @ManyToOne(() => Inventory, (inv) => inv.replenishment, { eager: true })
  @JoinColumn()
  @Exclude()
  inventory: Inventory;

  @Expose()
  get employeeFullName(): string {
    return `${this.employee.firstName} ${this.employee.lastName}`;
  }

  @Expose()
  get inventoryName(): string {
    return this.inventory?.name;
  }
}
