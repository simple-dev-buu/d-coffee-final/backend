import { Expose } from 'class-transformer';
import { Attendance } from 'src/attendances/entities/attendance.entity';
import { Bill } from 'src/bills/entities/bill.entity';
import { Branch } from 'src/branches/entities/branch.entity';
import { Payroll } from 'src/payrolls/entities/payroll.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Replenishment } from 'src/replenishment/entities/replenishment.entity';
import { StockTaking } from 'src/stock-taking/entities/stock-taking.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  tel: string;

  @Column()
  gender: string;

  @Column()
  birthDay: Date;

  @Column()
  title: string;

  @Column()
  qualification: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column({ default: 0 })
  moneyRate: number;

  @Column({ default: 0 })
  minWork: number;

  @Column()
  startDate: Date;

  @Column({ nullable: true })
  endDate: Date;

  @OneToMany(() => Bill, (bill) => bill.employee)
  bills: Bill[];

  @OneToMany(() => Replenishment, (rep) => rep.employee)
  replenishment: Replenishment[];

  @OneToMany(() => StockTaking, (stk) => stk.employee)
  stockTakingList: StockTaking[];

  @ManyToOne(() => Branch, (branch) => branch.employees, { eager: true })
  branch: Branch;

  @OneToMany(() => Payroll, (payroll) => payroll.employee)
  payrolls: Payroll[];

  @OneToMany(() => Attendance, (attendance) => attendance.employee)
  attendance: Attendance[];

  @OneToMany(() => Receipt, (rec) => rec.employee)
  receipts: Receipt[];

  @Expose()
  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}
