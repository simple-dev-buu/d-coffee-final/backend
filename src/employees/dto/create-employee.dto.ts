import { IsNotEmpty, IsString } from 'class-validator';

export class CreateEmployeeDto {
  // @IsString()
  // role: string;

  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsString()
  @IsNotEmpty()
  tel: string;

  @IsString()
  @IsNotEmpty()
  gender: string;

  @IsNotEmpty()
  birthDay: Date;

  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  qualification: string;

  moneyRate: string;

  minWork: string;

  startDate: string;

  endDate: string;

  image: string;
}
