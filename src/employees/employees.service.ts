import { Injectable } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Branch } from 'src/branches/entities/branch.entity';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private readonly employeeRepo: Repository<Employee>,
    @InjectRepository(User)
    private readonly userRepo: Repository<User>,
    @InjectRepository(Branch) private readonly branchRepo: Repository<Branch>,
  ) {}

  async create(createEmployeeDto: CreateEmployeeDto): Promise<Employee> {
    console.log(createEmployeeDto.birthDay)
    const emp = new Employee();
    emp.firstName = createEmployeeDto.firstName;
    emp.lastName = createEmployeeDto.lastName;
    emp.tel = createEmployeeDto.tel;
    emp.gender = createEmployeeDto.gender;
    emp.birthDay = new Date(createEmployeeDto.birthDay);
    emp.title = createEmployeeDto.title;
    emp.qualification = createEmployeeDto.qualification;
    emp.moneyRate = parseFloat(createEmployeeDto.moneyRate);
    emp.minWork = parseInt(createEmployeeDto.minWork);
    emp.startDate = new Date(createEmployeeDto.startDate);
    emp.endDate = new Date(createEmployeeDto.endDate);
    if (createEmployeeDto.image && createEmployeeDto.image !== '') {
      emp.image = createEmployeeDto.image;
    }
    return this.employeeRepo.save(emp);
  }

  findAll(): Promise<Employee[]> {
    return this.employeeRepo.find();
  }

  async findAllByBranchId(branchId: number) {
    const branch = await this.branchRepo.findOneBy({ id: branchId });
    return await this.employeeRepo.find({ where: { branch: branch } });
  }

  async findAttendent(month: number, empId: number, year: number) {
    return await this.employeeRepo.query(
      `CALL getAttendenceInMonthInYear(${month}, ${empId}, ${year});`,
    );
  }

  findOne(id: number) {
    return this.employeeRepo.findOneBy({ id: id });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    console.log(updateEmployeeDto.birthDay)
    const updateEmployee = await this.employeeRepo.findOneBy({ id });
    updateEmployee.firstName = updateEmployeeDto.firstName;
    updateEmployee.lastName = updateEmployeeDto.lastName;
    updateEmployee.tel = updateEmployeeDto.tel;
    updateEmployee.gender = updateEmployeeDto.gender;
    updateEmployee.birthDay = new Date(updateEmployeeDto.birthDay);
    console.log('updateBirthDay'+updateEmployee.birthDay)
    updateEmployee.title = updateEmployeeDto.title;
    updateEmployee.qualification = updateEmployeeDto.qualification;
    updateEmployee.moneyRate = parseFloat(updateEmployeeDto.moneyRate);
    updateEmployee.minWork = parseInt(updateEmployeeDto.minWork);
    updateEmployee.startDate = new Date(updateEmployeeDto.startDate);
    updateEmployee.endDate = new Date(updateEmployeeDto.endDate);
    if (updateEmployeeDto.image && updateEmployeeDto.image !== '') {
      updateEmployee.image = updateEmployeeDto.image;
    }
    this.employeeRepo.save(updateEmployee);

    const result = await this.employeeRepo.findOne({
      where: { id: id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteEmployee = await this.employeeRepo.findOneBy({ id });
    return this.employeeRepo.remove(deleteEmployee);
  }
}
