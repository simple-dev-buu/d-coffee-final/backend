import { Module } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from './entities/employee.entity';
import { User } from 'src/users/entities/user.entity';
import { Branch } from 'src/branches/entities/branch.entity';
import { Payroll } from 'src/payrolls/entities/payroll.entity';
import { Attendance } from 'src/attendances/entities/attendance.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Employee, User, Branch, Payroll, Attendance]),
  ],
  controllers: [EmployeesController],
  providers: [EmployeesService],
})
export class EmployeesModule {}
