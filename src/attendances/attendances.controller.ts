import { Controller, Get, Post, Body, Patch, Param } from '@nestjs/common';
import { AttendancesService } from './attendances.service';
import { CreateAttendanceDto } from './dto/create-attendance.dto';
import { UpdateAttendanceDto } from './dto/update-attendance.dto';

@Controller('attendances')
export class AttendancesController {
  constructor(private readonly attendancesService: AttendancesService) {}

  @Post()
  create(@Body() createAttendanceDto: CreateAttendanceDto) {
    return this.attendancesService.create(createAttendanceDto);
  }

  @Get()
  findAll() {
    return this.attendancesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.attendancesService.findOne(+id);
  }

  @Get('/clockIn/:clockIn')
  findOneByClockIn(@Param('clockIn') clockIn: string) {
    console.log('Attendance Controller');
    console.log(clockIn);
    return this.attendancesService.findOneByClockIn(clockIn);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateAttendanceDto: UpdateAttendanceDto,
  ) {
    console.log('Attendance Controller');
    console.log(Object.values(updateAttendanceDto));
    return this.attendancesService.update(+id, updateAttendanceDto);
  }
}
