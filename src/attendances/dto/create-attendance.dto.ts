import { IsBoolean, IsInt, IsNotEmpty, IsString } from 'class-validator';

export class CreateAttendanceDto {
  @IsNotEmpty()
  emp_ID: number;
  @IsNotEmpty()
  clockIn: string;
  @IsNotEmpty()
  clockOut: string;
  @IsInt()
  timeWorked: number;
  @IsString()
  typeWork: string;
  @IsBoolean()
  approved: boolean;
}
