import { PartialType } from '@nestjs/swagger';
import { CreateAttendanceDto } from './create-attendance.dto';
import { IsBoolean, IsInt, IsNotEmpty, IsString } from 'class-validator';

export class UpdateAttendanceDto extends PartialType(CreateAttendanceDto) {
  @IsNotEmpty()
  emp_ID: number;
  @IsNotEmpty()
  clockOut: string;
  @IsInt()
  timeWorked: number;
  @IsString()
  typeWork: string;
  @IsBoolean()
  approved: boolean;
}
