import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Attendance {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  clockIn: string;
  @Column({ nullable: true })
  clockOut: string;
  @CreateDateColumn()
  createdDate: Date;
  @Column({ default: 'missing' })
  type: string;
  @Column({ default: 0 })
  timeWorked: number;
  @Column({ default: false })
  approved: boolean;
  @ManyToOne(() => Employee, (employee) => employee.attendance)
  employee: Employee;
}
