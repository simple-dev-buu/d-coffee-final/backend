import { Injectable } from '@nestjs/common';
import { CreateAttendanceDto } from './dto/create-attendance.dto';
import { UpdateAttendanceDto } from './dto/update-attendance.dto';
import { Attendance } from './entities/attendance.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Employee } from 'src/employees/entities/employee.entity';

@Injectable()
export class AttendancesService {
  constructor(
    @InjectRepository(Attendance)
    private attendancesRepository: Repository<Attendance>,
    @InjectRepository(Employee) private employeeRepo: Repository<Employee>,
  ) {}
  async create(createAttendanceDto: CreateAttendanceDto) {
    const emp = await this.employeeRepo.findOneByOrFail({
      id: createAttendanceDto.emp_ID,
    });
    const attendance = new Attendance();
    attendance.employee = emp;
    attendance.clockIn = createAttendanceDto.clockIn;
    attendance.approved = createAttendanceDto.approved;
    console.log(attendance);
    return this.attendancesRepository.save(attendance);
  }

  findAll() {
    return this.attendancesRepository.find({ order: { id: 'DESC' } });
  }

  findOne(id: number) {
    return this.attendancesRepository.findOneBy({ id });
  }

  findOneByClockIn(clockIn: string) {
    return this.attendancesRepository.findOneByOrFail({ clockIn: clockIn });
  }

  async update(id: number, updateAttendanceDto: UpdateAttendanceDto) {
    console.log('Backend Service');
    const updateAttendance = await this.attendancesRepository.findOne({
      where: { clockIn: updateAttendanceDto.clockIn, id: id },
    });
    console.log(Object.values(updateAttendance));
    updateAttendance.clockOut = updateAttendanceDto.clockOut;
    updateAttendance.timeWorked = updateAttendanceDto.timeWorked;
    updateAttendance.type = updateAttendanceDto.typeWork;
    this.attendancesRepository.save(updateAttendance);

    const result = await this.attendancesRepository.findOne({
      where: { id: updateAttendance.id },
    });
    console.log(Object.values(result));
    return result;
  }
}
