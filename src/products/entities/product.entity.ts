import { Category } from 'src/categories/entities/category.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { ReceiptItem } from 'src/receipts/entities/receipt-items.entity';
// import { Promotion } from "src/promotions/entities/promotion.entity";
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @CreateDateColumn()
  createdDate: Date;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @ManyToOne(() => Category, (category) => category.products)
  category: Category;

  @OneToMany(() => ReceiptItem, (receiptItem) => receiptItem.product)
  receiptItem: ReceiptItem[];

  @ManyToMany(() => Promotion, (promotion) => promotion.productsUsable)
  @JoinTable()
  promotions: Promotion[];
}
