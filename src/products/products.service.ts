import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';
import { Category } from 'src/categories/entities/category.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRepository: Repository<Product>,
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>,
  ) {}
  create(createProductDto: CreateProductDto) {
    const product = new Product();
    product.name = createProductDto.name;
    product.price = parseFloat(createProductDto.price);
    product.category = JSON.parse(createProductDto.category);
    if (createProductDto.image && createProductDto.image !== '') {
      product.image = createProductDto.image;
    }
    return this.productsRepository.save(product);
  }

  findAll() {
    return this.productsRepository.find({ relations: { category: true } });
  }

  async findAllByCategory(categoryId: number) {
    const category = await this.categoryRepository.findOneBy({
      id: categoryId,
    });
    return this.productsRepository.find({
      where: { category: category },
      relations: { category: true },
      order: { name: 'DESC' },
    });
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { id },
      relations: { category: true },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const updateProduct = await this.productsRepository.findOne({
      where: { id },
    });
    updateProduct.name = updateProductDto.name;
    updateProduct.price = parseFloat(updateProductDto.price);
    updateProduct.category = JSON.parse(updateProductDto.category);
    if (updateProductDto.image && updateProductDto.image !== '') {
      updateProduct.image = updateProductDto.image;
    }
    this.productsRepository.save(updateProduct);

    const result = await this.productsRepository.findOne({
      where: { id },
      relations: { category: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.productsRepository.findOneByOrFail({ id });
    await this.productsRepository.remove(deleteProduct);
    return deleteProduct;
  }
}
