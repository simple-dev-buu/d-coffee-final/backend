/* eslint-disable prettier/prettier */
import { Receipt } from 'src/receipts/entities/receipt.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  tel: string;

  @Column()
  birthDay: Date;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column({ default: 0 })
  point: number;

  @CreateDateColumn()
  registerDate: Date;

  @Column({ default: true })
  status: boolean;

  @OneToMany(() => Receipt, (receipt) => receipt.customer)
  receipt: Receipt[];
}
