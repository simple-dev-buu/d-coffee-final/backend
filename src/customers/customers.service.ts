/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
  ) {}

  create(dto: CreateCustomerDto) {
    const customer = this.customerRepository.create({ ...dto });
    if (dto.image && dto.image !== '') {
      customer.image = dto.image;
    }
    return this.customerRepository.save(customer);
  }

  // async createByQuery(createCustomerDto: CreateCustomerDto) {
  //   // const message = '@p1';
  //   // const result = '@p2';
  //   // return this.customerRepository.query(
  //   // `CALL createCustomer('${createCustomerDto.name}', '${createCustomerDto.tel}', '${createCustomerDto.birthDay}', '${message}', '${result}'); \nSELECT ${message} AS pMessage , ${result} AS pResult;`,
  //   // );
  //   const date = createCustomerDto.birthDay.split('T')[0];
  //   const query = `CALL createCustomer('${createCustomerDto.name}', '${createCustomerDto.tel}', '${date}', @p1, @p2);`;
  //   await this.customerRepository.query(query);
  //   const query2 = `SELECT @p1 AS pMessage, @p2 AS pResult;`;
  //   const result = await this.customerRepository.query(query2);
  //   console.log(result);
  // return result;
  // }

  findAll() {
    return this.customerRepository.find({ order: { id: 'DESC' } });
  }

  findOne(id: number) {
    return this.customerRepository.findOneByOrFail({ id });
  }

  findOneByTelephone(tel: string) {
    try {
      return this.customerRepository.findOneByOrFail({ tel: tel });
    } catch {
      return new NotFoundException();
    }
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const updateCus = await this.customerRepository.findOneByOrFail({ id });
    updateCus.name = updateCustomerDto.name;
    updateCus.tel = updateCustomerDto.tel;
    updateCus.birthDay = new Date(updateCustomerDto.birthDay);
    if (updateCustomerDto.image && updateCustomerDto.image !== '') {
      updateCus.image = updateCustomerDto.image;
    } else {
      updateCus.image = 'noimage.jpg';
    }
    this.customerRepository.save(updateCus);
    const result = await this.customerRepository.findOneBy({ id });
    return result;
  }

  async remove(id: number) {
    const deletedCustomer = await this.customerRepository.findOneByOrFail({
      id,
    });
    await this.customerRepository.remove(deletedCustomer);
    return deletedCustomer;
  }
}
