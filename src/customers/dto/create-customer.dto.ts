import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

/* eslint-disable prettier/prettier */
export class CreateCustomerDto {
  @IsString()
  name: string;
  @IsString()
  tel: string;
  @IsNotEmpty()
  birthDay: Date;
  @IsOptional()
  image?: string;
}
