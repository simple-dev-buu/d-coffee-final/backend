import { Branch } from 'src/branches/entities/branch.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ReceiptItem } from './receipt-items.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  totalUnit: number;

  @Column()
  totalBefore: number;

  @Column()
  discount: number;

  @Column()
  netPrice: number;

  @Column()
  receivedAmount: number;

  @Column()
  change: number;

  @Column()
  paymentType: string;

  @Column()
  givePoint: number;

  @Column()
  usePoint: number;

  @Column()
  totalPoint: number;

  @CreateDateColumn()
  createdDateTime: Date;

  @ManyToOne(() => Branch, (branch) => branch.receipts)
  branch: Branch;

  @ManyToOne(() => Customer, (customer) => customer.receipt)
  customer: Customer;

  @ManyToOne(() => Employee, (emp) => emp.receipts)
  employee: Employee;

  @ManyToOne(() => Promotion, (promotion) => promotion.receipt)
  promotion: Promotion;

  @OneToMany(() => ReceiptItem, (item) => item.receipt, {
    cascade: ['insert', 'update', 'remove'],
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  receiptItems: ReceiptItem[];
}
