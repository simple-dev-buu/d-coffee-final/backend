/* eslint-disable prettier/prettier */
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Receipt } from './receipt.entity';
import { Product } from 'src/products/entities/product.entity';

@Entity()
export class ReceiptItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  unit: number;

  @Column()
  total: number;

  @ManyToOne(() => Receipt, (receipt) => receipt.receiptItems)
  receipt: Receipt;

  @ManyToOne(() => Product, (product) => product.receiptItem)
  product: Product;
}
