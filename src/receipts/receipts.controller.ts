import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { ReceiptsService } from './receipts.service';
import { CreateReceiptDto } from './dto/create-receipt.dto';

@Controller('receipts')
export class ReceiptsController {
  constructor(private readonly receiptsService: ReceiptsService) {}

  @Post()
  create(@Body() createReceiptDto: CreateReceiptDto) {
    return this.receiptsService.create(createReceiptDto);
  }

  @Get()
  findAll() {
    return this.receiptsService.findAll();
  }

  @Post('/more')
  findMoreThanMoney(@Body() data: { money: number; branchId: number }) {
    return this.receiptsService.findMoreOrLessReceipt(
      'more',
      data.money,
      data.branchId,
    );
  }
  @Post('/less')
  findLessThanMoney(@Body() data: { money: number; branchId: number }) {
    return this.receiptsService.findMoreOrLessReceipt(
      'less',
      data.money,
      data.branchId,
    );
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.receiptsService.findOne(+id);
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateReceiptDto: UpdateReceiptDto) {
  //   return this.receiptsService.update(+id, updateReceiptDto);
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.receiptsService.remove(+id);
  }
}
