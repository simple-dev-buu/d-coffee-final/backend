import { IsArray, IsInt, IsNumber, IsString } from 'class-validator';

export class CreateReceiptDto {
  @IsArray()
  receiptItems: CreateReceiptItemDto[];

  @IsInt()
  empId: number;

  @IsInt()
  branchId: number;

  @IsInt()
  cusId: number;

  @IsInt()
  promotionId: number;

  @IsInt()
  totalUnit: number;

  @IsNumber()
  totalBefore: number;

  @IsNumber()
  discount: number;

  @IsNumber()
  netPrice: number;

  @IsNumber()
  receivedAmount: number;

  @IsNumber()
  change: number;

  @IsString()
  paymentType: string;

  @IsInt()
  givePoint: number;

  @IsInt()
  usePoint: number;

  @IsInt()
  totalPoint: number;
}
export class CreateReceiptItemDto {
  @IsInt()
  productId: number;

  @IsString()
  name: string;

  @IsNumber()
  price: number;

  @IsInt()
  unit: number;

  @IsNumber()
  total: number;
}
