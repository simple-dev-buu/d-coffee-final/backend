/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
// import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Repository } from 'typeorm';
import { Product } from 'src/products/entities/product.entity';
import { Branch } from 'src/branches/entities/branch.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { ReceiptItem } from './entities/receipt-items.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt) private receiptsRepository: Repository<Receipt>,
    @InjectRepository(ReceiptItem)
    private receiptItemsRepository: Repository<ReceiptItem>,
    @InjectRepository(Employee) private empRepo: Repository<Employee>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
    @InjectRepository(Branch)
    private readonly branchRepository: Repository<Branch>,
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
    @InjectRepository(Promotion)
    private readonly promotionRepository: Repository<Promotion>,
  ) {}
  async create(createReceiptDto: CreateReceiptDto) {
    const receipt = new Receipt();
    const emp = await this.empRepo.findOneByOrFail({
      id: createReceiptDto.empId,
    });
    const branch = await this.branchRepository.findOneByOrFail({
      id: createReceiptDto.branchId,
    });
    let existPromotion;
    if (createReceiptDto.promotionId > 0) {
      existPromotion = await this.promotionRepository.findOneBy({
        id: createReceiptDto.promotionId,
      });
    }
    let existCustomer;
    if (createReceiptDto.cusId > 0) {
      existCustomer = await this.customerRepository.findOneBy({
        id: createReceiptDto.cusId,
      });
    }
    receipt.customer = existCustomer;
    receipt.promotion = existPromotion;
    receipt.employee = emp;
    receipt.branch = branch;
    receipt.discount = createReceiptDto.discount;
    receipt.totalBefore = createReceiptDto.totalBefore;
    receipt.receivedAmount = createReceiptDto.receivedAmount;
    receipt.netPrice = createReceiptDto.netPrice;
    receipt.totalUnit = createReceiptDto.totalUnit;
    receipt.receiptItems = [];
    receipt.paymentType = createReceiptDto.paymentType;
    receipt.givePoint = createReceiptDto.givePoint;
    receipt.change = createReceiptDto.change;
    receipt.usePoint = createReceiptDto.usePoint
      ? createReceiptDto.usePoint
      : 0;
    receipt.totalPoint = createReceiptDto.totalPoint;
    for (let i = 0; i < createReceiptDto.receiptItems.length; i++) {
      const oi = createReceiptDto.receiptItems[i];
      console.log(oi);
      const receiptItem = new ReceiptItem();
      receiptItem.product = await this.productsRepository.findOneBy({
        id: oi.productId,
      });
      receiptItem.name = receiptItem.product.name;
      receiptItem.price = receiptItem.product.price;
      receiptItem.unit = oi.unit;
      receiptItem.total = oi.total;
      console.log('', receiptItem);
      await this.receiptItemsRepository.save(receiptItem);
      // console.log(res);
      console.log('Before push:', receipt.receiptItems); // Log before push
      receipt.receiptItems.push(receiptItem);
      console.log('After push:', receipt.receiptItems); // Log after push
    }
    console.log('ReceiptItem:' + receipt.receiptItems);
    if (receipt.customer) {
      console.log('Have Member');
      receipt.customer.point += receipt.totalPoint;
      console.log('Updated Point' + receipt.customer.point);
      await this.customerRepository.update(
        receipt.customer.id,
        receipt.customer,
      );
    }
    return this.receiptsRepository.save(receipt);
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: {
        receiptItems: true,
      },
      order: {
        id: 'desc',
      },
    });
  }

  async findMoreOrLessReceipt(
    moreOrless: string,
    money: number,
    branchId: number,
  ) {
    return await this.receiptsRepository.query(
      `Call getReceipts('${moreOrless}', ${money}, ${branchId})`,
    );
  }

  findOne(id: number) {
    return this.receiptsRepository.findOneOrFail({
      where: { id },
      relations: { receiptItems: true },
    });
  }

  async remove(id: number) {
    const deleteReceipt = await this.receiptsRepository.findOneOrFail({
      where: { id },
    });
    await this.receiptsRepository.remove(deleteReceipt);

    return deleteReceipt;
  }
}
